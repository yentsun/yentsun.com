foto-event.ru
=============

:date: 2012-07-02 10:04
:tags: python, pyramid, sqlite, html, jquery
:category: мои проекты
:slug: foto-event
:abstr: Сайт-галерея фотографа Дениса Дюдюева. Агрегация из Google+ и Flickr.
:lang: ru

.. image:: images/index-inner.jpg
   :alt: логотип foto-event.ru

:Тех. стек: Python, Pyramid, Flickr API, Google API, SQLite, HTML, jQuery
:Время: 2 месяца
:Репозиторий: недоступен
:Статус: онлайн: http://foto-event.ru/photosets.html

Разработал галерею будучи фрилансером, полностью "с нуля" (бэкенд,
БД, фронтенд) за исключением дизайна.

Реализовано:

* каталог фотоальбомов с Flickr API
* синдикация постов от Google+ API