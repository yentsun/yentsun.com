Магазин Инваск
==============

:date: 2011-04-02 10:04
:tags: php, zend_framework, mysql, html, jquery, online-shop
:category: мои проекты
:slug: invask
:abstr: Интернет-магазин музыкальных инструментов на Zend Framework, интеграция
        с 1С.
:lang: ru

.. image:: images/invask_logo.png
   :alt: логотип инваск

:Тех. стек: PHP, Zend Framework, `Whyte`_, MySQL, HTML, jQuery
:Время: ½ года
:Репозиторий: недоступен
:Статус: онлайн: http://invask.ru/

Разработал магазин будучи удаленным сотрудником, полностью "с нуля" (бэкенд,
БД, фронтенд) за исключением дизайна.

Реализовано:

* каталог с категориями
* корзина заказа
* отслеживание статуса заказа

.. note:: Я закончил участие в проекте в 2012 году - возможны значительные
          изменения на текущий момент.

.. _`Whyte`: {filename}/whyte.rst