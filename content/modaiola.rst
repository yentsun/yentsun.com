Магазин Modaiola.com
====================

:date: 2011-10-11 10:04
:tags: php, zend_framework, mysql, html, jquery, online-shop
:category: мои проекты
:slug: modaiola
:abstr: Интернет-магазин одежды на Zend Framework, интеграция с 1С.
:lang: ru

.. image:: images/modaiola_logo.gif
   :alt: логотип modaiola

:Тех. стек: PHP, Zend Framework, `Whyte`_, MySQL, Lucene, HTML, jQuery
:Время: 1 год
:Репозиторий: недоступен
:Статус: онлайн: http://modaiola.com

Разработал магазин будучи удаленным сотрудником, полностью "с нуля" (бэкенд,
БД, фронтенд) за исключением дизайна.

Реализовано:

* каталог с категориями
* корзина заказа
* отслеживание статуса заказа
* новостная подписка
* полнотекстовый поиск на Zend Lucene / PyLucene

.. note:: Я закончил участие в проекте в 2012 году - возможны значительные
          изменения на текущий момент.

.. _`Whyte`: {filename}/whyte.rst