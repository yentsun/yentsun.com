Контакты
========

:sort_order: 9
:slug: contacts
:lang: ru

E-mail, мессенджер, телефон и т. д.

:|email|: max@korinets.name, mkorinets@gmail.com
:|hangouts|: mkorinets@gmail.com
:|whatsapp|: +7 (926) 307-99-75
:|skype|: max.korinets

.. |email| image:: ../images/email.png
.. |hangouts| image:: ../images/hangouts.png
.. |whatsapp| image:: ../images/whatsapp.png
.. |skype| image:: ../images/skype.png