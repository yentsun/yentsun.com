Real Estate Utils
=================

:date: 2012-09-03 12:02
:tags: python, sqlalchemy, утилиты
:category: мои проекты
:slug: re-utils
:abstr: Для обработки и сохранения больших объемов информации о недвижимости,
        для манипуляций графикой (SVG), связанной с этой недвижимостью, а также
        для работы с именами файлов в формате `shorthand` я разработал этот
        пакет утилит
:lang: ru

Он стал своего рода ядром, на котором работает несколько других смежных
проектов:

* `Hydra <{filename}/hydra.rst>`_
* `Scroll <{filename}/scroll.rst>`_

:Язык: Python
:Репозиторий: https://bitbucket.org/yentsun/real_etsate_utils/src
:Версия: 0.3.9

Пакет утилит включает в себя:

* progressbar - функция для отображения шкалы прогресса в консоли
* parse_shorthand - обработка формата `shorthand`_
* process_value - "безопасное" считывание значения с форсированием нужного типа
  данных
* набор функций для преобразования координат SVG в координаты для тега `area`
* set_style_value - установка атрибутов тегам SVG (например для утолщения
  штриха в чертеже)
* набор функций для манипулирования масштабом изображений в SVG
* модели сущностей (SQLAlchemy) необходимых для обработки данных о недвижимости

.. _`shorthand`: http://art3d.ru/docs/shorthand.html