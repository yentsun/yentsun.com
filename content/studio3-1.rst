Studio 3:1
==========

:date: 2012-05-23 10:04
:tags: php, zend_framework, mysql, html, jquery
:category: мои проекты
:slug: studio3-1
:abstr: Сайт хоккейной статистики для профессионалов.
:lang: ru

.. image:: images/studio3-1_logo.png
   :alt: логотип Studio 3:1

:Тех. стек: PHP, Zend Framework, `Whyte`_, MySQL, HTML, jQuery
:Время: ½ года
:Репозиторий: https://bitbucket.org/yentsun/studio3-1
:Статус: неизвестен

Разработал сайт будучи фрилансером, полностью "с нуля" (бэкенд,
БД, фронтенд) за исключением дизайна.

Реализовано:

* каталог хоккейных лиг, команд, матчей
* архив видеоматериалов с метками важных моментов

.. _`Whyte`: {filename}/whyte.rst